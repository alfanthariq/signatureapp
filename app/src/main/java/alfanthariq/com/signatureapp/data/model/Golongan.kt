package alfanthariq.com.signatureapp.data.model

import androidx.room.*

@Entity(tableName = "golongan")
data class Golongan(
        @PrimaryKey()
        @ColumnInfo(name = "id")
        var id: Int = 0,
        @ColumnInfo(name = "nama")
        var nama: String = ""
)
{
    @Ignore
    constructor() : this(0)
}

@Dao
interface GolonganDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(gol: Golongan)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLists(lists: List<Golongan>)

    @Query("SELECT * FROM golongan ORDER BY id ASC")
    fun all(): List<Golongan>

    @Query("SELECT * FROM golongan WHERE id = :id")
    fun one(id : Int): Golongan?

    @Query("DELETE FROM golongan")
    fun deleteAll()
}