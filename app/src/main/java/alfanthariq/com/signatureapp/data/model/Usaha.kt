package alfanthariq.com.signatureapp.data.model

import androidx.room.*

@Entity(tableName = "usaha")
data class Usaha(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Int,
        @ColumnInfo(name = "nomor_surat")
        var nomor_surat: String = "",
        @ColumnInfo(name = "nama_usaha")
        var nama_usaha: String? = null,
        @ColumnInfo(name = "alamat_usaha")
        var alamat_usaha: String? = null,
        @ColumnInfo(name = "tahun_berdiri")
        var tahun_berdiri: String? = null
)
{
    @Ignore
    constructor() : this(0)
}

@Dao
interface UsahaDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: Usaha)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLists(lists: List<Usaha>)

    @Query("SELECT * FROM usaha ORDER BY nomor_surat DESC")
    fun all(): List<Usaha>

    @Query("SELECT * FROM usaha WHERE nomor_surat = :ns")
    fun one(ns : String): Usaha

    @Query("DELETE FROM usaha")
    fun deleteAll()
}