package alfanthariq.com.signatureapp.data.model

import androidx.room.*

@Entity(tableName = "kematian")
data class Kematian(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Int,
        @ColumnInfo(name = "nomor_surat")
        var nomor_surat: String = "",
        @ColumnInfo(name = "nama")
        var nama: String? = null,
        @ColumnInfo(name = "nik")
        var nik: String? = null,
        @ColumnInfo(name = "ttl")
        var ttl: String? = null,
        @ColumnInfo(name = "gender")
        var gender: Int = 0, // 0 : Laki-laki 1: perempuan
        @ColumnInfo(name = "alamat")
        var alamat: String = "",
        @ColumnInfo(name = "hubungan")
        var hubungan: String = "",
        @ColumnInfo(name = "tgl_meninggal")
        var tgl_meninggal: String = "",
        @ColumnInfo(name = "lokasi_meninggal")
        var lokasi_meninggal: String = "",
        @ColumnInfo(name = "sebab_meninggal")
        var sebab_meninggal: String = ""
)
{
    @Ignore
    constructor() : this(0)
}

@Dao
interface KematianDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: Kematian)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLists(lists: List<Kematian>)

    @Query("SELECT * FROM kematian ORDER BY nomor_surat DESC")
    fun all(): List<Kematian>

    @Query("SELECT * FROM kematian WHERE nomor_surat = :ns")
    fun one(ns : String): Kematian

    @Query("DELETE FROM kematian")
    fun deleteAll()
}