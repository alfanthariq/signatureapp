package alfanthariq.com.signatureapp.data.model

import androidx.room.*

@Entity(tableName = "master_surat")
data class MasterSurat(
        @PrimaryKey()
        @ColumnInfo(name = "id")
        var id: Int = 0,
        @ColumnInfo(name = "nama")
        var nama: String = ""
)
{
    @Ignore
    constructor() : this(0)
}

@Dao
interface MasterSuratDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(surat: MasterSurat)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLists(lists: List<MasterSurat>)

    @Query("SELECT * FROM master_surat ORDER BY id ASC")
    fun all(): List<MasterSurat>

    @Query("SELECT * FROM master_surat WHERE id = :id ORDER BY nama DESC")
    fun one(id : Int): MasterSurat

    @Query("DELETE FROM master_surat")
    fun deleteAll()
}