package alfanthariq.com.signatureapp.data.remote

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @GET("list/surat/{golongan}/{desa_id}")
    fun getSurat(@Path("golongan") golongan: Int,
                 @Path("desa_id") desa_id: Int,
                 @Query("auth_token") token : String): Call<ResponseBody>

    @GET("list/surat/{tr_id}")
    fun getSurat(@Path("tr_id") id: Int,
                 @Query("auth_token") token : String): Call<ResponseBody>

    @GET("print/{tr_id}")
    fun print(@Path("tr_id") id: Int,
              @Query("auth_token") token : String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("update-ttd/")
    fun updateTTD(@Field("nik") nik: String,
                  @Field("userid") userid: Int,
                  @Field("ttd_img") base64img: String,
                  @Query("auth_token") token : String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("submit/surat/")
    fun approveSurat(@Field("tr_id") tr_id: Int,
                     @Field("golongan") golongan: Int,
                     @Field("nik") nik: String,
                     @Field("ttd_img") ttd: String,
                     @Query("auth_token") token : String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("reject/")
    fun rejectSurat(@Field("tr_id") tr_id: Int,
                    @Field("catatan") catatan: String,
                    @Query("auth_token") token : String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("login/")
    fun login(
            @Field("username") username: String,
            @Field("password") password: String): Call<ResponseBody>


    /*@FormUrlEncoded
    @POST("validate/lembur/")
    fun validateLembur(
            @Field("nip") nip: String,
            @Field("tgl_awal") tgl_awal: String,
            @Field("tgl_akhir") tgl_akhir: String): Call<ResponseBody>*/
}