package alfanthariq.com.signatureapp.data.model

class SuratList {
    var id = 0
    var nomorSurat = ""
    var tgl = ""
    var type = ""
    var dataType = 0
    var status = 0
    var bulanTahun = ""
    var catatan = ""
    var nik = ""
    var progress = 0f
    var nama = ""
    var verTo = ""
}