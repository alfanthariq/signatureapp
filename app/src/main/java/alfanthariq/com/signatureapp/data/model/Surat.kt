package alfanthariq.com.signatureapp.data.model

import androidx.room.*

@Entity(tableName = "surat")
data class Surat(
        @PrimaryKey()
        @ColumnInfo(name = "id")
        var id: Int = 0,
        @ColumnInfo(name = "nomor_surat")
        var nomor_surat: String = "",
        @ColumnInfo(name = "nama")
        var nama: String = "",
        @ColumnInfo(name = "nik")
        var nik: String? = null,
        @ColumnInfo(name = "desa")
        var desa: String = "",
        @ColumnInfo(name = "tanggal")
        var tanggal: String? = null,
        @ColumnInfo(name = "formentry")
        var formentry: String = "",
        @ColumnInfo(name = "st_persyaratan")
        var st_persyaratan: String? = null,
        @ColumnInfo(name = "st_ttd")
        var st_ttd: String? = null,
        @ColumnInfo(name = "routing")
        var routing: String = "",
        @ColumnInfo(name = "ttd")
        var ttd: String = "",
        @ColumnInfo(name = "attach")
        var attach: String? = null,
        @ColumnInfo(name = "catatan")
        var catatan: String? = null,
        @ColumnInfo(name = "tipe")
        var tipe: Int = 0,
        @ColumnInfo(name = "tipe_name")
        var tipe_name: String? = null,
        @ColumnInfo(name = "status_approve")
        var status_approve: Int = 0
)
{
    @Ignore constructor() : this(0)
}

@Dao
interface SuratDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(surat: Surat)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLists(lists: List<Surat>)

    @Query("SELECT * FROM surat ORDER BY tanggal DESC")
    fun all(): List<Surat>

    @Query("SELECT * FROM surat WHERE id = :id")
    fun one(id : Int): Surat

    @Query("UPDATE surat SET status_approve = :status WHERE id = :id")
    fun updateStatus(id : Int, status : Int) //0:uncomfirmed, 1:approved, 2:rejected

    @Query("UPDATE surat SET catatan = :cat WHERE id = :id")
    fun updateCatatan(id : Int, cat : String)

    @Query("SELECT * FROM surat WHERE nomor_surat = :ns AND tipe = :tipe ORDER BY tanggal DESC")
    fun oneByTipe(ns : String, tipe : Int): Surat

    @Query("SELECT * FROM surat WHERE tipe = :t AND status_approve = :status ORDER BY tanggal DESC")
    fun byTipe(t : Int, status : Int): List<Surat>

    @Query("SELECT * FROM surat WHERE tipe = :t ORDER BY tanggal DESC")
    fun byTipeAll(t : Int): List<Surat>

    @Query("DELETE FROM surat")
    fun deleteAll()

    @Query("SELECT * FROM surat WHERE (nomor_surat LIKE :keyword) ORDER BY tanggal DESC")
    fun search(keyword : String): List<Surat>

    @Query("SELECT * FROM surat WHERE tipe = :tipe AND ((nama LIKE :keyword) OR (nik LIKE :keyword) OR (strftime('%d/%m/%Y', tanggal) LIKE :keyword)) ORDER BY tanggal DESC")
    fun search(tipe : Int, keyword : String): List<Surat>
}