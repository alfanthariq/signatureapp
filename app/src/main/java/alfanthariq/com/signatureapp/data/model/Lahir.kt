package alfanthariq.com.signatureapp.data.model

import androidx.room.*

@Entity(tableName = "lahir")
data class Lahir(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Int,
        @ColumnInfo(name = "nomor_surat")
        var nomor_surat: String = "",
        @ColumnInfo(name = "nama_bayi")
        var nama_bayi: String? = null,
        @ColumnInfo(name = "hubungan")
        var hubungan: String? = null,
        @ColumnInfo(name = "ttl")
        var ttl: String? = null,
        @ColumnInfo(name = "tempat_lahir")
        var tempat_lahir: String? = null,
        @ColumnInfo(name = "gender")
        var gender: Int = 0, // 0 : Laki-laki 1: perempuan
        @ColumnInfo(name = "alamat_ibu")
        var alamat: String = "",
        @ColumnInfo(name = "nama_ibu")
        var nama_ibu: String = "",
        @ColumnInfo(name = "nama_ayah")
        var nama_ayah: String = ""
)
{
    @Ignore
    constructor() : this(0)
}

@Dao
interface LahirDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: Lahir)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLists(lists: List<Lahir>)

    @Query("SELECT * FROM lahir ORDER BY nomor_surat DESC")
    fun all(): List<Lahir>

    @Query("SELECT * FROM lahir WHERE nomor_surat = :ns")
    fun one(ns : String): Lahir

    @Query("DELETE FROM lahir")
    fun deleteAll()
}