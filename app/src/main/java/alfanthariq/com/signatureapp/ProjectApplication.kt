package alfanthariq.com.signatureapp

import alfanthariq.com.signatureapp.data.local.AppDatabase
import android.app.Application
import android.content.Context
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class ProjectApplication : Application() {

    companion object {
        var database: AppDatabase? = null

        fun getDB(context: Context) : AppDatabase?{
            ProjectApplication.database = AppDatabase.getInstance(context)
            return AppDatabase.getInstance(context)
        }
    }

    override fun onCreate() {
        super.onCreate()

        getDB(this)
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
    }
}