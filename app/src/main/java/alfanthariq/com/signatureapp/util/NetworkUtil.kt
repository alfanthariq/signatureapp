package alfanthariq.com.signatureapp.util

import android.app.DownloadManager
import android.content.Context
import android.content.Context.DOWNLOAD_SERVICE
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Environment
import retrofit2.HttpException
import java.net.URL
import android.webkit.MimeTypeMap
import android.webkit.URLUtil
import android.webkit.URLUtil.guessFileName




object NetworkUtil {

    /**
     * Returns true if the Throwable is an instance of RetrofitError with an
     * http status code equals to the given one.
     */
    val local = "http://192.168.1.37:8080/"
    val office = "http://10.8.12.188:8080/"
    val trial = "http://api.alfanthariq.com/efikaliber42/public/"
    val live = "http://api.sembodobalongbendo.id/"
    var useAPI = live
    val PROFILE_IMG_BASE_URL = "http://sembodobalongbendo.id/Uploads/"

    fun getPhotoURL(context: Context) : String {
        val pref_setting: SharedPreferences by lazy {
            context.getSharedPreferences("setting.conf", Context.MODE_PRIVATE)
        }

        val url = URL(pref_setting.getString("url_api", "http://api.sembodobalongbendo.id/"))
        val url_port = if (url.port != -1) {
            url.port.toString()
        } else {
            ""
        }
        val baseUrl = url.protocol + "://" + url.host + ":" + url_port
        val photoUrl = "$baseUrl/dkapi/web/"

        return photoUrl
    }

    fun getUrlAPI(context: Context) {
        val pref_setting: SharedPreferences by lazy {
            context.getSharedPreferences("setting.conf", Context.MODE_PRIVATE)
        }

        useAPI = pref_setting.getString("url", "http://api.sembodobalongbendo.id/")
    }

    fun isHttpStatusCode(throwable: Throwable, statusCode: Int): Boolean {
        return throwable is HttpException && throwable.code() == statusCode
    }

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    fun downloadFile(context: Context, url: String) {
        val r = DownloadManager.Request(Uri.parse(url))
        val nameOfFile = guessFileName(url, null,
                MimeTypeMap.getFileExtensionFromUrl(url))
        r.setDescription(nameOfFile)
        r.setTitle(nameOfFile)
        r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,nameOfFile)
        r.allowScanningByMediaScanner()
        r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)

        val dm = context.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
        dm.enqueue(r)
    }
}