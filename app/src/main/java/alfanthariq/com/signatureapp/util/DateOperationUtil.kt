package alfanthariq.com.signatureapp.util

import android.app.Dialog
import android.content.Context
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import com.livinglifetechway.k4kotlin.onClick
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateOperationUtil {

    fun getCurrentTimeStr(formatStr : String) : String {
        val format = SimpleDateFormat(formatStr, Locale.US)
        val cal = Calendar.getInstance()
        val tgl = format.format(cal.time)

        return tgl
    }

    fun dateAdd(date : Date, day : Int) : Date {
        val gc1 = GregorianCalendar()
        gc1.time = date
        gc1.add(Calendar.DATE, day)

        return gc1.time
    }

    fun strToDate(formatStr : String, str : String) : Date? {
        val dateFormat = SimpleDateFormat(formatStr, Locale.US)
        val convertedDate: Date
        try {
            convertedDate = dateFormat.parse(str)
            return convertedDate
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return null
    }

    fun getTimeStr(formatStr : String, date : Date) : String {
        val format = SimpleDateFormat(formatStr, Locale.US)
        val tgl = format.format(date)

        return tgl
    }

    fun dateStrFormat(formatInput : String, formatOutput : String, date : String) : String {
        val dateFormat = SimpleDateFormat(formatInput, Locale.US)
        val convertedDate: Date
        try {
            convertedDate = dateFormat.parse(date)
            val newformat = SimpleDateFormat(formatOutput, Locale.US)
            val finalDateString = newformat.format(convertedDate)
            return finalDateString
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return ""
    }
}