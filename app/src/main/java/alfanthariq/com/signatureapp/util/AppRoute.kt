package alfanthariq.com.signatureapp.util

import alfanthariq.com.signatureapp.features.approval.ApprovalActivity
import alfanthariq.com.signatureapp.features.approval.detail.DetailSuratActivity
import alfanthariq.com.signatureapp.features.login.LoginActivity
import alfanthariq.com.signatureapp.features.main.MainActivity
import alfanthariq.com.signatureapp.features.main.profile.ProfileActivity
import android.app.Activity
import android.content.Context
import android.content.Intent

object AppRoute {
    fun open(context : Context, page : String){
        var intent = Intent()
        when (page) {
            "approval" -> intent = Intent(context, ApprovalActivity::class.java)
            "main" -> intent = Intent(context, MainActivity::class.java)
            "login" -> intent = Intent(context, LoginActivity::class.java)
            "profile" -> intent = Intent(context, ProfileActivity::class.java)
            "detail" -> intent = Intent(context, DetailSuratActivity::class.java)
        }

        context.startActivity(intent)
    }

    fun openWithParam(context : Context, page : String, param : HashMap<String, String>){
        var intent = Intent()
        when (page) {
            "approval" -> intent = Intent(context, ApprovalActivity::class.java)
            "main" -> intent = Intent(context, MainActivity::class.java)
            "login" -> intent = Intent(context, LoginActivity::class.java)
            "profile" -> intent = Intent(context, ProfileActivity::class.java)
            "detail" -> intent = Intent(context, DetailSuratActivity::class.java)
        }

        param.forEach {
            intent.putExtra(it.key, it.value)
        }
        context.startActivity(intent)
    }

    fun openResult(activity : Activity, page : String, requestCode : Int){
        var intent = Intent()
        when (page) {
            "approval" -> intent = Intent(activity.baseContext, ApprovalActivity::class.java)
            "main" -> intent = Intent(activity.baseContext, MainActivity::class.java)
            "login" -> intent = Intent(activity.baseContext, LoginActivity::class.java)
            "profile" -> intent = Intent(activity.baseContext, ProfileActivity::class.java)
            "detail" -> intent = Intent(activity.baseContext, DetailSuratActivity::class.java)
            /*"cuti" -> intent = Intent(activity.baseContext, CutiActivity::class.java)
            "izin" -> intent = Intent(activity.baseContext, IzinActivity::class.java)
            "dinas" -> intent = Intent(activity.baseContext, DinasActivity::class.java)
            "add_cuti" -> intent = Intent(activity.baseContext, AddCutiActivity::class.java)
            "add_izin" -> intent = Intent(activity.baseContext, AddIzinActivity::class.java)
            "add_dinas" -> intent = Intent(activity.baseContext, AddDinasActivity::class.java)
            "setting" -> intent = Intent(activity.baseContext, SettingActivity::class.java)
            "absen_dinas" -> intent = Intent(activity.baseContext, AbsenDinasActivity::class.java)
            "approval" -> intent = Intent(activity.baseContext, ApprovalActivity::class.java)
            "absensi" -> intent = Intent(activity.baseContext, AbsensiActivity::class.java)
            "lembur" -> intent = Intent(activity.baseContext, LemburActivity::class.java)
            "add_lembur" -> intent = Intent(activity.baseContext, AddLemburActivity::class.java)
            "profile" -> intent = Intent(activity.baseContext, ProfileActivity::class.java)
            "finger_status" -> intent = Intent(activity.baseContext, FingerStatusActivity::class.java)*/
        }

        activity.startActivityForResult(intent, requestCode)
    }
}