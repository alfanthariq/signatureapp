package alfanthariq.com.signatureapp.util

import alfanthariq.com.signatureapp.ProjectApplication
import alfanthariq.com.signatureapp.data.local.AppDatabase

object LogUtil {
    val db: AppDatabase by lazy {
        ProjectApplication.database!!
    }
}