package alfanthariq.com.signatureapp.features.main

import alfanthariq.com.signatureapp.R
import alfanthariq.com.signatureapp.data.local.AppDatabase
import alfanthariq.com.signatureapp.data.model.MasterSurat
import alfanthariq.com.signatureapp.util.gone
import alfanthariq.com.signatureapp.util.visible
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.livinglifetechway.k4kotlin.onClick
import kotlinx.android.synthetic.main.item_dashboard_surat.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class DashboardSuratAdapter(private var surats: ArrayList<MasterSurat>,
                            private val clickListener: (MasterSurat) -> Unit
    ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        val vh : RecyclerView.ViewHolder
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_dashboard_surat, p0, false)
        val db = AppDatabase.getInstance(p0.context)!!
        vh = MainHolder(v,db)
        return vh
    }

    override fun getItemCount(): Int = surats.size

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (p0 is MainHolder){
            p0.bind(surats[p1], clickListener)
        }
    }

    class MainHolder(private val view: View, val db : AppDatabase) : RecyclerView.ViewHolder(view) {
        fun bind(data: MasterSurat, listener: (MasterSurat) -> Unit) {
            doAsync {
                val tipe = data.id
                val all = db.suratDAO().byTipeAll(tipe)
                val approved = db.suratDAO().byTipe(tipe, 1)

                uiThread {
                    view.txt_nama_surat.text = data.nama
                    if (all.isNotEmpty()) {
                        val persen = (approved.size / all.size.toFloat()) * 100
                        view.progress.progress = persen
                        view.txt_progress.text =  "${approved.size} approved / ${all.size} data"
                    } else {
                        view.progress.progress = 0f
                        view.txt_progress.text =  "0 approved / 0 data"
                    }

                    if (approved.size != all.size) {
                        view.view_badge.visible()
                    } else {
                        view.view_badge.gone()
                    }

                    view.container.onClick {
                        listener(data)
                    }
                }
            }
        }
    }
}