package alfanthariq.com.signatureapp.features.login

import alfanthariq.com.signatureapp.features.base.BaseMvpView
import alfanthariq.com.signatureapp.features.base.BasePresenter

object LoginContract {
    interface View : BaseMvpView {

    }

    interface Presenter : BasePresenter<View> {
        fun doLogin(username : String, password : String, token : String,
                    callback : (Boolean, String) -> Unit)
    }
}