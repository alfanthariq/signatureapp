package alfanthariq.com.signatureapp.features.main.imageviewer

import alfanthariq.com.signatureapp.features.base.BasePresenterImpl

class ImageViewerPresenter (var view: ImageViewerContract.View) :
        BasePresenterImpl<ImageViewerContract.View>(), ImageViewerContract.Presenter {
}