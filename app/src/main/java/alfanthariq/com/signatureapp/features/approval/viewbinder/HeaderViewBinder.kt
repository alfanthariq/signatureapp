package alfanthariq.com.signatureapp.features.approval.viewbinder

import alfanthariq.com.signatureapp.R
import alfanthariq.com.signatureapp.data.model.SuratList
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_header_list.view.*
import me.drakeet.multitype.ItemViewBinder

class HeaderViewBinder : ItemViewBinder<SuratList, HeaderViewBinder.MainHolder>() {
    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): MainHolder {
        val vh : RecyclerView.ViewHolder
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_header_list, parent, false)
        vh = MainHolder(v)
        return vh
    }

    override fun onBindViewHolder(holder: MainHolder, item: SuratList) {
        holder.bind(item)
    }

    class MainHolder(private val view: View) : RecyclerView.ViewHolder(view){
        fun bind(title: SuratList){
            view.txt_header_title.text = title.bulanTahun
        }
    }
}