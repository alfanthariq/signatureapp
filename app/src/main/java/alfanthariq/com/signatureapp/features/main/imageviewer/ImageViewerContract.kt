package alfanthariq.com.signatureapp.features.main.imageviewer

import alfanthariq.com.signatureapp.features.base.BaseMvpView
import alfanthariq.com.signatureapp.features.base.BasePresenter

object ImageViewerContract {
    interface View : BaseMvpView {

    }

    interface Presenter : BasePresenter<View> {

    }
}