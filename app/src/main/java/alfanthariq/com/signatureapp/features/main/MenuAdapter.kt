package alfanthariq.com.signatureapp.features.main

import alfanthariq.com.signatureapp.R
import alfanthariq.com.signatureapp.data.model.MasterSurat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.livinglifetechway.k4kotlin.onClick
import kotlinx.android.synthetic.main.menu_item.view.*

class MenuAdapter(private var menu_ids: ArrayList<MasterSurat>,
                  private val clickListener: (Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        val vh : RecyclerView.ViewHolder
        val v = LayoutInflater.from(p0.context).inflate(R.layout.menu_item, p0, false)
        vh = MainHolder(v)
        return vh
    }

    override fun getItemCount(): Int = menu_ids.size

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (p0 is MainHolder){
            p0.bind(menu_ids[p1], clickListener)
        }
    }

    class MainHolder(private val view: View) : RecyclerView.ViewHolder(view){

        fun bind(menu_id : MasterSurat, listener: (Int) -> Unit){
            /*when (menu_id) {
                1 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Keterangan Umum"
                }
                2 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Keterangan Janda / Duda"
                }
                3 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Keterangan Tidak Mampu"
                }
                4 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Keterangan Kematian"
                }
                5 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Keterangan Keluarga Miskin"
                }
                6 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Keterangan Belum Menikah"
                }
                7 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Keterangan Lahir"
                }
                8 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Keterangan Bepergian"
                }
                9 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Keterangan Domisil Usaha"
                }
                10 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Keterangan Domisili Penduduk"
                }
                11 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Pengantar KK"
                }
                12 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Pengantar KTP"
                }
                13 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Pengantar TDP"
                }
                14 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Pengantar SIUP"
                }
                15 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Izin Mendirikan Bangunan"
                }
                16 -> {
                    //view.btn_menu.setImageResource(R.mipmap.ic_cuti)
                    view.txt_title_menu.text = "Surat Pindah Domisili"
                }
            }*/

            view.txt_title_menu.text = menu_id.nama

            view.btn_menu.onClick {
                listener(menu_id.id)
            }
        }
    }
}