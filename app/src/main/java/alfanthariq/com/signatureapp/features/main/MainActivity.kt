package alfanthariq.com.signatureapp.features.main

import alfanthariq.com.signatureapp.R
import alfanthariq.com.signatureapp.data.model.MasterSurat
import alfanthariq.com.signatureapp.features.base.BaseActivity
import alfanthariq.com.signatureapp.features.common.ErrorView
import alfanthariq.com.signatureapp.util.AppRoute
import alfanthariq.com.signatureapp.util.NetworkUtil
import alfanthariq.com.signatureapp.util.PreferencesHelper
import alfanthariq.com.signatureapp.util.gone
import android.Manifest
import android.content.Context
import android.content.ContextWrapper
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Environment
import androidx.recyclerview.widget.GridLayoutManager
import android.view.View
import com.livinglifetechway.k4kotlin.onClick
import com.livinglifetechway.k4kotlin.toast
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.livinglifetechway.quickpermissions_kotlin.util.QuickPermissionsOptions
import com.livinglifetechway.quickpermissions_kotlin.util.QuickPermissionsRequest
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.main_menu_circle.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class MainActivity : BaseActivity<MainContract.View,
        MainPresenter>(),
        MainContract.View, ErrorView.ErrorListener {

    var pref_setting : SharedPreferences? = null
    var pref_profile : SharedPreferences? = null
    private var menuAdapter : MenuAdapter? = null
    private var menus = ArrayList<Int>()
    private var dashboardSuratAdapter : DashboardSuratAdapter? = null
    private var surats = ArrayList<MasterSurat>()

    override var mPresenter: MainPresenter
        get() = MainPresenter(this)
        set(value) {}

    override fun layoutId(): Int = R.layout.activity_main

    override fun getTagClass(): String = javaClass.simpleName

    override fun onReloadData() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pref_setting = PreferencesHelper.getSettingPref(this)
        pref_profile = PreferencesHelper.getProfilePref(this)

        methodWithPermissions()

        init()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onBackPressed() {
        if (sliding_menu.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            sliding_menu.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        } else {
            super.onBackPressed()
        }
    }

    fun init(){
        sliding_menu.panelHeight = 0
        sliding_menu.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED

        if (pref_setting!!.getBoolean("is_permited", false)) {
            showLoadingDialog("Mengunduh surat ...")
            mPresenter.downloadSurat {
                hideLoadingDialog()
                mPresenter.loadSurat {
                    surats.clear()
                    surats.addAll(it)
                    setupRecyclerDashboardSurat()
                }
                if (!it) {
                    toast("Gagal mengunduh data surat")
                }
            }
        }

        img_hamburger.gone()

        //NetworkUtil.getUrlAPI(this)

        img_setting.onClick {
            AppRoute.open(this@MainActivity, "profile")
        }

//        menus.add(1)
//        menus.add(2)
//        menus.add(3)
//        menus.add(4)
//        menus.add(5)
//        menus.add(6)
//        menus.add(7)
//        menus.add(8)
//        menus.add(9)
//        menus.add(10)
//        menus.add(11)
//        menus.add(12)
//        menus.add(13)
//        menus.add(14)
//        menus.add(15)
//        menus.add(16)
        //setupRecycler()

        img_hamburger.onClick {
            if (sliding_menu.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                sliding_menu.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            } else {
                sliding_menu.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
            }
        }

        img_logout.onClick {
            mPresenter.logout {
                AppRoute.open(this@MainActivity, "login")
                finish()
            }
        }

        sliding_menu.addPanelSlideListener(object : SlidingUpPanelLayout.PanelSlideListener{
            override fun onPanelSlide(panel: View?, slideOffset: Float) {

            }

            override fun onPanelStateChanged(panel: View?, previousState: SlidingUpPanelLayout.PanelState?, newState: SlidingUpPanelLayout.PanelState?) {
                if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    sliding_menu.panelHeight = 0
                }
            }

        })

        swipe_refresh.setOnRefreshListener {
            mPresenter.downloadSurat {
                swipe_refresh.isRefreshing = false
                setupRecyclerDashboardSurat()
            }
        }
    }

    override fun onResume() {
        mPresenter.loadProfile{ nama, gol, desa ->
            txt_nama.text = nama
            txt_nik.text = desa
            txt_jabatan.text = gol
        }

        //NetworkUtil.getUrlAPI(this)

        setupRecyclerDashboardSurat()
        super.onResume()
    }

    fun setupRecyclerDashboardSurat(){
        dashboardSuratAdapter = DashboardSuratAdapter(surats){
            val param = HashMap<String, String>()
            param["tipe"] = it.id.toString()
            AppRoute.openWithParam(this, "approval", param)
        }

        rec_dashboard_surat.apply {
            val gridLayout = GridLayoutManager(this@MainActivity, 2, GridLayoutManager.VERTICAL, false)
            layoutManager = gridLayout
            adapter = dashboardSuratAdapter
        }
    }

    /*fun setupRecycler(){
        val param = HashMap<String, String>()
        menuAdapter = MenuAdapter(menus) {
            param["tipe"] = it.toString()
            when (it) {
                1 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                2 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                3 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                4 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                5 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                6 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                7 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                8 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                9 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                10 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                11 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                12 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                13 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                14 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                15 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
                16 -> {
                    AppRoute.openWithParam(this, "approval", param)
                }
            }
        }

        recycler_menu.apply {
            val gridLayout = GridLayoutManager(this@MainActivity, 3, GridLayoutManager.VERTICAL, false)
            layoutManager = gridLayout
            adapter = menuAdapter
        }
    }*/

    val quickPermissionsOption = QuickPermissionsOptions(
            false,
            "Custom rational message",
            true,
            "Custom permanently denied message",
            { rationaleCallback(it) },
            { permissionsPermanentlyDenied(it) }
    )

    fun methodWithPermissions() = runWithPermissions(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE, options = quickPermissionsOption) {
        showLoadingDialog("Mengunduh surat ...")
        mPresenter.downloadSurat {
            hideLoadingDialog()
            mPresenter.loadSurat {
                surats.clear()
                surats.addAll(it)
                setupRecyclerDashboardSurat()
            }
            if (!it) {
                toast("Gagal mengunduh data surat")
            }
        }

        val editor = pref_setting!!.edit()
        editor.putBoolean("is_permited", true)
        editor.apply()
    }

    fun runTimePermission() = runWithPermissions(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE, options = quickPermissionsOption) {
    }

    private fun rationaleCallback(req: QuickPermissionsRequest) {
        // this will be called when permission is denied once or more time. Handle it your way
        androidx.appcompat.app.AlertDialog.Builder(this)
                .setTitle("Permissions Denied")
                .setMessage("This is the custom rationale dialog. Please allow us to proceed " + "asking for permissions again, or cancel to end the permission flow.")
                .setPositiveButton("Go Ahead") { dialog, which -> req.proceed() }
                .setNegativeButton("cancel") { dialog, which -> req.cancel() }
                .setCancelable(false)
                .show()
    }

    private fun permissionsPermanentlyDenied(req: QuickPermissionsRequest) {
        // this will be called when some/all permissions required by the method are permanently
        // denied. Handle it your way.
        androidx.appcompat.app.AlertDialog.Builder(this)
                .setTitle("Permissions Denied")
                .setMessage("This is the custom permissions permanently denied dialog. " +
                        "Please open app settings to open app settings for allowing permissions, " +
                        "or cancel to end the permission flow.")
                .setPositiveButton("App Settings") { dialog, which -> req.openAppSettings() }
                .setNegativeButton("Cancel") { dialog, which -> req.cancel() }
                .setCancelable(false)
                .show()
    }
}
