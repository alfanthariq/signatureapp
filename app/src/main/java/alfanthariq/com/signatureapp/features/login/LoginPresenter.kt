package alfanthariq.com.signatureapp.features.login

import alfanthariq.com.signatureapp.data.remote.ApiClient
import alfanthariq.com.signatureapp.data.remote.ApiService
import alfanthariq.com.signatureapp.features.base.BasePresenterImpl
import alfanthariq.com.signatureapp.util.NetworkUtil
import alfanthariq.com.signatureapp.util.PreferencesHelper
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class LoginPresenter (var view: LoginContract.View) :
        BasePresenterImpl<LoginContract.View>(), LoginContract.Presenter {

    var apiService: ApiService? = null
    val context = view.getContext()
    val pref_setting = PreferencesHelper.getSettingPref(context)
    val pref_profile = PreferencesHelper.getProfilePref(context)

    override fun doLogin(username: String, password: String, token: String, callback : (Boolean, String) -> Unit) {
        apiService = ApiClient.getClient(context,null, NetworkUtil.useAPI)
                .create(ApiService::class.java)
        val sendApi = apiService?.login(username, password)
        sendApi?.enqueue(object : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                callback(false, "Request Failed")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val json: String = response.body()!!.string()
                    val obj: JsonObject = JsonParser().parse(json).asJsonObject
                    val data = obj.get("data").asJsonObject
                    val token_auth = if (obj.has("token")) {
                        obj.get("token").asString
                    } else {
                        ""
                    }

                    val editor_profile = pref_profile.edit()
                    editor_profile.putInt("user_id", data.get("id").asInt)
                    editor_profile.putString("nama", data.get("username").asString)
                    editor_profile.putInt("golongan_id", data.get("id_grup").asInt)
                    editor_profile.putString("golongan_str", data.get("nama_golongan").asString)
                    editor_profile.putInt("desa_id", data.get("ds_id").asInt)
                    editor_profile.putString("desa_str", data.get("nama_desa").asString)
                    editor_profile.putString("email", data.get("email").asString)
                    editor_profile.putString("token", token_auth)
                    editor_profile.apply()

                    val editor = pref_setting.edit()
                    editor.putBoolean("is_login", true)
                    editor.apply()

                    callback(true, "")
                } else {
                    try{
                        val json: String = response.errorBody()!!.string()
                        val obj: JsonObject = JsonParser().parse(json).asJsonObject
                        val err_msg = obj.get("err_message").asString
                        callback(false, err_msg)
                    } catch (e : Exception) {
                        callback(false, "Internal Server Error")
                    }
                }
            }
        })
    }
}