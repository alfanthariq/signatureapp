package alfanthariq.com.signatureapp.features.approval

import alfanthariq.com.signatureapp.data.local.AppDatabase
import alfanthariq.com.signatureapp.data.model.*
import alfanthariq.com.signatureapp.data.remote.ApiClient
import alfanthariq.com.signatureapp.data.remote.ApiService
import alfanthariq.com.signatureapp.features.base.BasePresenterImpl
import alfanthariq.com.signatureapp.util.DateOperationUtil
import alfanthariq.com.signatureapp.util.NetworkUtil
import alfanthariq.com.signatureapp.util.PreferencesHelper
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import okhttp3.ResponseBody
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class ApprovalPresenter (var view: ApprovalContract.View) :
        BasePresenterImpl<ApprovalContract.View>(), ApprovalContract.Presenter {

    val context = view.getContext()
    var apiService: ApiService? = null
    val db = AppDatabase.getInstance(context)!!

    val pref_profile = PreferencesHelper.getProfilePref(context)

    val token = pref_profile.getString("token", "")

    override fun loadSurat(tipe: Int) {
        doAsync {
            val datas = ArrayList<SuratList>()

            val surat = db.suratDAO().byTipeAll(tipe)

            surat.forEach {
                val data = SuratList()
                val bulanTahunData = DateOperationUtil.dateStrFormat("yyyy-MM-dd", "MMM yyyy", it.tanggal!!)

                val tgl = DateOperationUtil.dateStrFormat("yyyy-MM-dd", "dd/MM/yyyy", it.tanggal!!)

                val dataRoute = it.routing
                val obj_route = JSONObject(dataRoute)
                val keys = obj_route.keys()

                var yesCount = 0
                var totalCount = 0
                var n = "0"
                while (keys.hasNext()) {
                    val key = keys.next()
                    val value = "${obj_route.get(key)}"
                    if (value == "Y") {
                        yesCount+=1
                    } else {
                        if (n=="0") n = key
                    }
                    totalCount += 1
                }

                val gol = db.golonganDAO().one(n.toInt())
                val verto = gol?.nama ?: "selesai"

                val progress = (yesCount / totalCount.toFloat()) * 100

                if (datas.isEmpty()) {
                    val dataDump = SuratList()
                    dataDump.id = -1
                    dataDump.bulanTahun = bulanTahunData
                    dataDump.dataType = 0
                    datas.add(dataDump)

                    data.id = it.id
                    data.nomorSurat = it.nomor_surat
                    data.tgl = tgl
                    data.type = it.tipe_name!!
                    data.status = it.status_approve
                    data.bulanTahun = bulanTahunData
                    data.dataType = 1
                    data.progress = progress
                    data.catatan = it.catatan!!
                    data.nik = it.nik!!
                    data.nama = it.nama
                    data.verTo = verto
                    datas.add(data)
                } else {
                    if (bulanTahunData != datas[datas.lastIndex].bulanTahun) {
                        val dataDump = SuratList()
                        dataDump.id = -1
                        dataDump.bulanTahun = bulanTahunData
                        dataDump.dataType = 0
                        datas.add(dataDump)

                        data.id = it.id
                        data.nomorSurat = it.nomor_surat
                        data.tgl = tgl
                        data.type = it.tipe_name!!
                        data.status = it.status_approve
                        data.bulanTahun = bulanTahunData
                        data.dataType = 1
                        data.progress = progress
                        data.catatan = it.catatan!!
                        data.nik = it.nik!!
                        data.nama = it.nama
                        data.verTo = verto
                        datas.add(data)
                    } else {
                        data.id = it.id
                        data.nomorSurat = it.nomor_surat
                        data.tgl = tgl
                        data.type = it.tipe_name!!
                        data.status = it.status_approve
                        data.bulanTahun = bulanTahunData
                        data.dataType = 1
                        data.progress = progress
                        data.catatan = it.catatan!!
                        data.nik = it.nik!!
                        data.nama = it.nama
                        data.verTo = verto
                        datas.add(data)
                    }
                }
            }

            uiThread {
                view.onLoadSurat(datas)
            }
        }
    }

    override fun searchSurat(tipe: Int, keyword: String) {
        doAsync {
            val datas = ArrayList<SuratList>()

            val surat = db.suratDAO().search(tipe, keyword)

            surat.forEach {
                val data = SuratList()
                val bulanTahunData = DateOperationUtil.dateStrFormat("yyyy-MM-dd", "MMM yyyy", it.tanggal!!)

                val tgl = DateOperationUtil.dateStrFormat("yyyy-MM-dd", "dd/MM/yyyy", it.tanggal!!)

                val dataRoute = it.routing
                val obj_route = JSONObject(dataRoute)
                val keys = obj_route.keys()

                var yesCount = 0
                var totalCount = 0
                var n = "0"
                while (keys.hasNext()) {
                    val key = keys.next()
                    val value = "${obj_route.get(key)}"
                    if (value == "Y") {
                        yesCount+=1
                    } else {
                        if (n=="0") n = key
                    }
                    totalCount += 1
                }

                val gol = db.golonganDAO().one(n.toInt())
                val verto = gol?.nama ?: "selesai"

                val progress = ((yesCount / totalCount) * 100).toFloat()

                if (datas.isEmpty()) {
                    val dataDump = SuratList()
                    dataDump.id = -1
                    dataDump.bulanTahun = bulanTahunData
                    dataDump.dataType = 0
                    datas.add(dataDump)

                    data.id = it.id
                    data.nomorSurat = it.nomor_surat
                    data.tgl = tgl
                    data.type = it.tipe_name!!
                    data.status = it.status_approve
                    data.bulanTahun = bulanTahunData
                    data.dataType = 1
                    data.progress = progress
                    data.catatan = it.catatan!!
                    data.nik = it.nik!!
                    data.nama = it.nama
                    data.verTo = verto
                    datas.add(data)
                } else {
                    if (bulanTahunData != datas[datas.lastIndex].bulanTahun) {
                        val dataDump = SuratList()
                        dataDump.id = -1
                        dataDump.bulanTahun = bulanTahunData
                        dataDump.dataType = 0
                        datas.add(dataDump)

                        data.id = it.id
                        data.nomorSurat = it.nomor_surat
                        data.tgl = tgl
                        data.type = it.tipe_name!!
                        data.status = it.status_approve
                        data.bulanTahun = bulanTahunData
                        data.dataType = 1
                        data.progress = progress
                        data.catatan = it.catatan!!
                        data.nik = it.nik!!
                        data.nama = it.nama
                        data.verTo = verto
                        datas.add(data)
                    } else {
                        data.id = it.id
                        data.nomorSurat = it.nomor_surat
                        data.tgl = tgl
                        data.type = it.tipe_name!!
                        data.status = it.status_approve
                        data.bulanTahun = bulanTahunData
                        data.dataType = 1
                        data.progress = progress
                        data.catatan = it.catatan!!
                        data.nik = it.nik!!
                        data.nama = it.nama
                        data.verTo = verto
                        datas.add(data)
                    }
                }
            }

            uiThread {
                view.onLoadSurat(datas)
            }
        }
    }

    override fun print(tr_id: Int) {
        apiService = ApiClient.getClient(context,null, NetworkUtil.useAPI)
                .create(ApiService::class.java)
        val sendApi = apiService?.print(tr_id, token)
        sendApi?.enqueue(object : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                view.onSuccessPrint("")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    println("response success")
                    val json: String = response.body()!!.string()
                    val obj: JsonObject = JsonParser().parse(json).asJsonObject
                    val url_file = obj.get("data").asString

                    view.onSuccessPrint(url_file)
                } else {
                    view.onSuccessPrint("")
                }
            }
        })
    }
}