package alfanthariq.com.signatureapp.features.approval.viewbinder

import alfanthariq.com.signatureapp.R
import alfanthariq.com.signatureapp.data.model.SuratList
import alfanthariq.com.signatureapp.util.PreferencesHelper
import alfanthariq.com.signatureapp.util.gone
import alfanthariq.com.signatureapp.util.visible
import me.drakeet.multitype.ItemViewBinder
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import com.livinglifetechway.k4kotlin.getColorCompat
import com.livinglifetechway.k4kotlin.onClick
import kotlinx.android.synthetic.main.item_section_approval.view.*

class SectionViewBinder (private val clickListener: (SuratList) -> Unit,
                         private val optionListener: (SuratList, Int) -> Unit) :
        ItemViewBinder<SuratList, SectionViewBinder.MainHolder>() {

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): MainHolder {
        val vh : RecyclerView.ViewHolder
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_section_approval, parent, false)
        vh = MainHolder(v)
        return vh
    }

    override fun onBindViewHolder(holder: MainHolder, item: SuratList) {
        holder.bind(item, clickListener, optionListener)
    }

    class MainHolder(private val view: View) : RecyclerView.ViewHolder(view){
        fun bind(surat: SuratList, clickListener: (SuratList) -> Unit, opsiListener: (SuratList, Int) -> Unit){
            view.txt_nama.text = "Nama lengkap : ${surat.nama}"
            view.txt_nik.text = "NIK : ${surat.nik}"
            view.txt_tgl.text = "Tanggal : ${surat.tgl}"
            view.txt_verto.text = "Verifikasi ke : ${surat.verTo}"
            when (surat.status) {
                0 -> {
                    view.txt_status.text = "Belum diresponse"
                    view.txt_status.setTextColor(view.context.getColorCompat(R.color.yellow_700))
                }
                1 -> {
                    view.txt_status.text = "Disetujui"
                    view.txt_status.setTextColor(view.context.getColorCompat(R.color.green_500))
                }
                2 -> {
                    view.txt_status.text = "Ditolak (${surat.catatan})"
                    view.txt_status.setTextColor(view.context.getColorCompat(R.color.red_300))
                }
                else -> ""
            }

            view.progress.progress = surat.progress
            view.txt_progress.text = "${surat.progress.toInt()} %"

            view.layout_item.setOnClickListener {
                clickListener(surat)
            }

            val pref_profile = PreferencesHelper.getProfilePref(view.context)
            val golongan = pref_profile.getInt("golongan_id", 1)

            if (golongan in arrayOf(2, 5, 17)) {
                if (surat.progress == 100f) {
                    view.btn_option.visible()
                } else {
                    view.btn_option.gone()
                }

                view.btn_option.onClick {
                    showMenu(view.btn_option, surat, opsiListener)
                }
            } else {
                view.btn_option.gone()
            }
        }

        fun showMenu(viewPop: View, ct: SuratList, opsiListener: (SuratList, Int) -> Unit) {
            val menu = PopupMenu(view.context, viewPop)
            menu.setOnMenuItemClickListener { item ->
                val id = item.itemId
                when (id) {
                    R.id.action_print -> opsiListener(ct, 0)
                }
                true
            }
            menu.inflate(R.menu.menu_surat)

            menu.show()
        }
    }
}