package alfanthariq.com.signatureapp.features.main.profile

import alfanthariq.com.signatureapp.features.base.BaseMvpView
import alfanthariq.com.signatureapp.features.base.BasePresenter
import android.graphics.Bitmap

object ProfileContract {
    interface View : BaseMvpView {

    }

    interface Presenter : BasePresenter<View>{
        fun updateTTD(ttdBitmap: Bitmap, callback : (Boolean, String) -> Unit)
    }
}