package alfanthariq.com.signatureapp.features.base

import android.content.Context

interface BasePresenter<in V : BaseMvpView> {

    fun attachView(view: V)

    fun detachView()

    fun getContext(context:Context)
}