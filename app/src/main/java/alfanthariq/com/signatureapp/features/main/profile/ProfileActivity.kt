package alfanthariq.com.signatureapp.features.main.profile

import alfanthariq.com.signatureapp.R
import alfanthariq.com.signatureapp.features.base.BaseActivity
import alfanthariq.com.signatureapp.features.common.ErrorView
import alfanthariq.com.signatureapp.util.FileUtil
import alfanthariq.com.signatureapp.util.GlideApp
import alfanthariq.com.signatureapp.util.NetworkUtil
import alfanthariq.com.signatureapp.util.PreferencesHelper
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import com.bumptech.glide.Glide
import com.livinglifetechway.k4kotlin.onClick
import com.livinglifetechway.k4kotlin.toast
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.dialog_ttd.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class ProfileActivity : BaseActivity<ProfileContract.View, ProfilePresenter>(),
            ProfileContract.View, ErrorView.ErrorListener{

    lateinit var pref_profile : SharedPreferences
    lateinit var pref_setting : SharedPreferences

    override var mPresenter: ProfilePresenter
        get() = ProfilePresenter(this)
        set(value) {}

    override fun layoutId(): Int = R.layout.activity_profile

    override fun getTagClass(): String = javaClass.simpleName

    override fun onReloadData() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pref_profile = PreferencesHelper.getProfilePref(this)
        pref_setting = PreferencesHelper.getSettingPref(this)

        init()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    fun init(){
        setToolbar(toolbar_profile, "", true)
        toolbar_profile.setNavigationIcon(R.mipmap.ic_back)

        loadData()

        btn_edit_ttd.onClick {
            openEditTTD()
        }

        edit_url.setText(pref_setting.getString("url", NetworkUtil.local))

        btn_simpan.onClick {
            val editor = pref_setting.edit()
            editor.putString("url", edit_url.text.toString())
            editor.apply()
            //NetworkUtil.getUrlAPI(this@ProfileActivity)
            toast("Simpan berhasil")
        }
    }

    fun loadData(){
        txt_nama_profil.text = pref_profile.getString("nama", "")
        txt_nik_profil.text = pref_profile.getString("golongan_str", "")
        txt_desa_profil.text = pref_profile.getString("desa_str", "-")

        doAsync {
            val base64ttd = pref_profile.getString("ttd_base64", "")!!
            if (base64ttd.isNotEmpty()) {
                val imgTTD = FileUtil.convert(base64ttd)
                uiThread {
                    GlideApp.with(this@ProfileActivity)
                            .load(imgTTD)
                            .into(img_ttd)
                }
            }
        }
    }

    fun openEditTTD(){
        val factory = LayoutInflater.from(this@ProfileActivity)
        val ttdView = factory.inflate(R.layout.dialog_ttd, null)
        val ttdDialog = AlertDialog.Builder(this@ProfileActivity).create()
        ttdDialog.setView(ttdView)
        ttdDialog.setCancelable(true)
        ttdView.btn_simpan_ttd.onClick {
            if (!ttdView.signature_pad.isEmpty) {
                showLoadingDialog("Uploading data ...")
                mPresenter.updateTTD(ttdView.signature_pad.signatureBitmap){ status, err_msg ->
                    if (status) {
                        hideLoadingDialog()
                        toast("Berhasil update template tanda tangan")
                        loadData()
                        ttdDialog.dismiss()
                    } else {
                        toast(err_msg)
                    }
                }
            } else {
                toast("Anda belum memasukkan tanda tangan")
            }
        }

        ttdDialog.show()
    }
}
