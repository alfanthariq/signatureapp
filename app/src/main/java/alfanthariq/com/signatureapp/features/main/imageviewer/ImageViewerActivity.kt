package alfanthariq.com.signatureapp.features.main.imageviewer

import alfanthariq.com.signatureapp.R
import alfanthariq.com.signatureapp.features.base.BaseActivity
import alfanthariq.com.signatureapp.features.common.ErrorView
import alfanthariq.com.signatureapp.util.GlideApp
import alfanthariq.com.signatureapp.util.gone
import alfanthariq.com.signatureapp.util.visible
import android.graphics.drawable.Drawable
import android.os.Bundle
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_image_viewer.*

class ImageViewerActivity : BaseActivity<ImageViewerContract.View,
        ImageViewerPresenter>(),
        ImageViewerContract.View, ErrorView.ErrorListener {

    override var mPresenter: ImageViewerPresenter
        get() = ImageViewerPresenter(this)
        set(value) {}

    override fun layoutId(): Int = R.layout.activity_image_viewer

    override fun getTagClass(): String = javaClass.simpleName

    override fun onReloadData() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setToolbar(toolbar_viewer, "", true)
        toolbar_viewer.setNavigationIcon(R.mipmap.ic_back)

        val url = intent.getStringExtra("url")
        val title = intent.getStringExtra("title")

        txt_title_viewer.text = title

        progress.visible()

        GlideApp.with(this)
                .load(url)
                .listener(object : RequestListener<Drawable>{
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        progress.gone()
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        progress.gone()
                        return false
                    }
                })
                .into(images)
    }
}
