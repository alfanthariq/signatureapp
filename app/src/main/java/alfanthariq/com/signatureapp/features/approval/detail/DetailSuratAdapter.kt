package alfanthariq.com.signatureapp.features.approval.detail

import alfanthariq.com.signatureapp.R
import alfanthariq.com.signatureapp.features.main.imageviewer.ImageViewerActivity
import alfanthariq.com.signatureapp.util.GlideApp
import alfanthariq.com.signatureapp.util.NetworkUtil
import alfanthariq.com.signatureapp.util.gone
import alfanthariq.com.signatureapp.util.visible
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.livinglifetechway.k4kotlin.onClick
import kotlinx.android.synthetic.main.item_detail_surat.view.*
import android.content.Intent
import android.net.Uri


class DetailSuratAdapter(private var detail: ArrayList<HashMap<String, String>>,
                         private val clickListener: (Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        val vh : RecyclerView.ViewHolder
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_detail_surat, p0, false)
        vh = DetailSuratAdapter.MainHolder(v)
        return vh
    }

    override fun getItemCount(): Int = detail.size

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (p0 is MainHolder){
            p0.bind(detail[p1], clickListener)
        }
    }

    class MainHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(data: HashMap<String, String>, listener: (Int) -> Unit) {

            val key = data["key"]?.replace('_', ' ')
            view.txt_key.text = key
            val value = data["value"]!!
            val extension = if ("." in value) {
                value.substring(value.lastIndexOf("."))
            } else ""

            println("Ext : $extension")

            when (extension.toLowerCase()) {
                ".jpg" -> {
                    view.txt_value.gone()
                    view.txt_pdf.gone()
                    view.imgPreview.visible()
                    GlideApp.with(view.context)
                            .load(NetworkUtil.PROFILE_IMG_BASE_URL+value)
                            .into(view.imgPreview)
                    view.imgPreview.onClick {
                        val intent = Intent(view.context, ImageViewerActivity::class.java)
                        intent.putExtra("url", NetworkUtil.PROFILE_IMG_BASE_URL+value)
                        intent.putExtra("title", key)
                        view.context.startActivity(intent)
                    }
                }
                ".png" -> {
                    view.txt_value.gone()
                    view.txt_pdf.gone()
                    view.imgPreview.visible()
                    GlideApp.with(view.context)
                            .load(NetworkUtil.PROFILE_IMG_BASE_URL+value)
                            .into(view.imgPreview)
                    view.imgPreview.onClick {
                        val intent = Intent(view.context, ImageViewerActivity::class.java)
                        intent.putExtra("url", NetworkUtil.PROFILE_IMG_BASE_URL+value)
                        intent.putExtra("title", key)
                        view.context.startActivity(intent)
                    }
                }
                ".jpeg" -> {
                    view.txt_value.gone()
                    view.txt_pdf.gone()
                    view.imgPreview.visible()
                    GlideApp.with(view.context)
                            .load(NetworkUtil.PROFILE_IMG_BASE_URL+value)
                            .into(view.imgPreview)
                    view.imgPreview.onClick {
                        val intent = Intent(view.context, ImageViewerActivity::class.java)
                        intent.putExtra("url", NetworkUtil.PROFILE_IMG_BASE_URL+value)
                        intent.putExtra("title", key)
                        view.context.startActivity(intent)
                    }
                }
                ".pdf" -> {
                    view.txt_value.gone()
                    view.txt_pdf.visible()
                    view.imgPreview.gone()
                    view.txt_pdf.onClick {
                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(NetworkUtil.PROFILE_IMG_BASE_URL+value))
                        view.context.startActivity(browserIntent)
                    }
                }
                ".doc" -> {
                    view.txt_value.gone()
                    view.txt_pdf.visible()
                    view.imgPreview.gone()
                    view.txt_pdf.onClick {
                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(NetworkUtil.PROFILE_IMG_BASE_URL+value))
                        view.context.startActivity(browserIntent)
                    }
                }
                else -> {
                    view.txt_value.visible()
                    view.txt_pdf.gone()
                    view.imgPreview.gone()
                    view.txt_value.text = value
                }
            }
        }
    }

}