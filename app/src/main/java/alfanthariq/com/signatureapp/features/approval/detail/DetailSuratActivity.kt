package alfanthariq.com.signatureapp.features.approval.detail

import alfanthariq.com.signatureapp.R
import alfanthariq.com.signatureapp.features.base.BaseActivity
import alfanthariq.com.signatureapp.features.common.ErrorView
import alfanthariq.com.signatureapp.util.*
import android.content.SharedPreferences
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.InputType
import android.view.View.VISIBLE
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.livinglifetechway.k4kotlin.onClick
import com.livinglifetechway.k4kotlin.toast
import kotlinx.android.synthetic.main.activity_detail_surat.*
import org.json.JSONArray
import org.json.JSONObject

class DetailSuratActivity : BaseActivity<DetailSuratContract.View, DetailSuratPresenter>(),
        DetailSuratContract.View, ErrorView.ErrorListener{

    private var detailAdapter : DetailSuratAdapter? = null
    private var details = ArrayList<HashMap<String, String>>()
    private var surat_id : String = ""
    private var surat_nik : String = ""
    private lateinit var pref_profile : SharedPreferences

    override var mPresenter: DetailSuratPresenter
        get() = DetailSuratPresenter(this)
        set(value) {}

    override fun layoutId(): Int = R.layout.activity_detail_surat

    override fun getTagClass(): String = javaClass.simpleName

    override fun onReloadData() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        surat_id = intent.getStringExtra("surat_id")
        surat_nik = intent.getStringExtra("surat_nik")
        pref_profile = PreferencesHelper.getProfilePref(this)

        init()
    }

    fun init(){
        setToolbar(toolbar_approval, "", true)
        toolbar_approval.setNavigationIcon(R.mipmap.ic_back)

        mPresenter.getData(surat_id.toInt()) {
            details.clear()

            val mapData = HashMap<String, String>()
            mapData["key"] = "Nama"
            mapData["value"] = it.nama
            details.add(mapData)

            val mapData1 = HashMap<String, String>()
            mapData1["key"] = "NIK"
            mapData1["value"] = it.nik!!
            details.add(mapData1)

            val mapData2 = HashMap<String, String>()
            mapData2["key"] = "Desa"
            mapData2["value"] = it.desa
            details.add(mapData2)

            // Formentry
            val data = it.formentry
            val obj = JSONObject(data)
            val keys = obj.keys()

            while (keys.hasNext()) {
                val key = keys.next()
                val mapData = HashMap<String, String>()
                mapData["key"] = key
                mapData["value"] = "${obj.get(key)}"
                details.add(mapData)
            }

            // Attach
            val data_a = it.attach
            val obj_a = JSONObject(data_a)
            val keys_a = obj_a.keys()

            while (keys_a.hasNext()) {
                val key = keys_a.next()
                val mapData = HashMap<String, String>()
                mapData["key"] = key
                mapData["value"] = "${obj_a.get(key)}"
                details.add(mapData)
            }

            if (it.status_approve == 1) {
                btn_submit.gone()
                btn_reject.gone()
                container_image.gone()
                container_sign.gone()
            } else {
                btn_submit.visible()
                btn_reject.visible()
                container_image.gone()
                container_sign.visible()
            }

            if (it.st_ttd != null) {
                val jsonStr = it.st_ttd?.replace("\\","")
                val ttdArr = JSONArray(jsonStr)
                val ttdList = mutableListOf<String>()
                for (i in 0 until ttdArr.length()) {
                    ttdList.add(ttdArr[i].toString())
                }

                if (ttdList.contains(pref_profile.getString("golongan_str", ""))) {
                    container_sign.visible()
                } else {
                    container_sign.gone()
                }
            }

            if (it.ttd != "" && it.ttd != "[]") {
                val jsonStr = it.ttd.replace("\\","")
                val obj_ttd = JSONObject(jsonStr)
                if (obj_ttd.has(pref_profile.getInt("golongan_id", 0).toString())) {
                    val ttd = obj_ttd.getString(pref_profile.getInt("golongan_id", 0).toString())
                    GlideApp.with(this@DetailSuratActivity)
                            .load(NetworkUtil.PROFILE_IMG_BASE_URL+ttd)
                            .into(img_ttd)
                }
            }

            setupRecycler()
        }

        btn_submit.onClick {
            if (container_sign.visibility == VISIBLE){
                if (!signature_pad.isEmpty) {
                    showLoadingDialog("Mengirim data ...")
                    mPresenter.approved(surat_id.toInt(), surat_nik, signature_pad.signatureBitmap){status, err_msg ->
                        hideLoadingDialog()
                        if (!status) {
                            toast(err_msg)
                        } else {
                            finish()
                        }
                    }
                } else {
                    toast("Anda belum melakukan tanda tangan")
                }
            } else {
                showLoadingDialog("Mengirim data ...")
                mPresenter.approved(surat_id.toInt(), surat_nik, signature_pad.signatureBitmap){status, err_msg ->
                    hideLoadingDialog()
                    if (!status) {
                        toast(err_msg)
                    } else {
                        finish()
                    }
                }
            }
        }

        btn_reject.onClick {
            var text = ""
            MaterialDialog(this@DetailSuratActivity).show {
                title(R.string.ket_reject)
                input(hint = "Tulis keterangan ...", inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                { dialog, _text ->
                    text = _text.toString()
                }
                positiveButton {
                    showLoadingDialog("Mengirim catatan ...")
                    mPresenter.reject(surat_id.toInt(), text) { status, err_msg ->
                        hideLoadingDialog()
                        if (!status) {
                            toast(err_msg)
                        } else {
                            finish()
                        }
                    }
                }
            }
        }
    }

    fun setupRecycler(){
        detailAdapter = DetailSuratAdapter(details){

        }

        rec_detail.apply {
            val linLayout = LinearLayoutManager(this@DetailSuratActivity, LinearLayoutManager.VERTICAL, false)
            layoutManager = linLayout
            adapter = detailAdapter
        }
    }
}
