package alfanthariq.com.signatureapp.features.main

import alfanthariq.com.signatureapp.data.local.AppDatabase
import alfanthariq.com.signatureapp.data.model.*
import alfanthariq.com.signatureapp.data.remote.ApiClient
import alfanthariq.com.signatureapp.data.remote.ApiService
import alfanthariq.com.signatureapp.features.base.BasePresenterImpl
import alfanthariq.com.signatureapp.util.DateOperationUtil
import alfanthariq.com.signatureapp.util.NetworkUtil
import alfanthariq.com.signatureapp.util.PreferencesHelper
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import okhttp3.ResponseBody
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter (var view: MainContract.View) :
        BasePresenterImpl<MainContract.View>(), MainContract.Presenter {

    val context = view.getContext()
    val pref_setting = PreferencesHelper.getSettingPref(context)
    val pref_profile = PreferencesHelper.getProfilePref(context)
    var apiService: ApiService? = null
    val db: AppDatabase by lazy {
        AppDatabase.getInstance(context)!!
    }
    val golongan = pref_profile.getInt("golongan_id", 1)
    val desa_id = pref_profile.getInt("desa_id", 11)

    val token = pref_profile.getString("token", "")

    override fun logout(callback: (Boolean) -> Unit){
        val editor = pref_setting.edit()
        editor.putBoolean("is_login", false)
        editor.apply()

        callback(true)
    }

    override fun downloadSurat(callback: (Boolean) -> Unit) {
        apiService = ApiClient.getClient(context,null, NetworkUtil.useAPI)
                .create(ApiService::class.java)
        val sendApi = apiService?.getSurat(golongan, desa_id, token)
        sendApi?.enqueue(object : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                callback(false)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    println("response success")
                    val json: String = response.body()!!.string()
                    val obj: JsonObject = JsonParser().parse(json).asJsonObject

                    insertSurat(obj, callback)
                } else {
                    callback(false)
                }
            }
        })
    }

    fun insertSurat(obj : JsonObject, callback: (Boolean) -> Unit){
        doAsync {
            db.masterSuratDAO().deleteAll()
            val data_ms = obj.get("master_surat").asJsonArray
            val ms = ArrayList<MasterSurat>()
            data_ms.forEach {
                val masterSr = MasterSurat()
                masterSr.id = it.asJsonObject.get("st_id").asInt
                masterSr.nama = it.asJsonObject.get("st_nama").asString

                ms.add(masterSr)
            }
            db.masterSuratDAO().insertLists(ms)

            db.golonganDAO().deleteAll()
            val data_gol = obj.get("golongan").asJsonArray
            val gol = ArrayList<Golongan>()
            data_gol.forEach {
                val masterGl = Golongan()
                masterGl.id = it.asJsonObject.get("id_golongan").asInt
                masterGl.nama = it.asJsonObject.get("Nama_Golongan").asString

                gol.add(masterGl)
            }
            db.golonganDAO().insertLists(gol)

            db.suratDAO().deleteAll()
            val data_sku = obj.get("data").asJsonArray
            val skus = ArrayList<Surat>()
            data_sku.forEach {
                val surat = Surat()
                surat.nomor_surat = it.asJsonObject.get("sr_name").asString
                surat.id = it.asJsonObject.get("tr_id").asInt
                surat.tanggal = DateOperationUtil.dateStrFormat("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", it.asJsonObject.get("tr_tglcreated").asString)
                surat.nik = if (it.asJsonObject.get("NIK").isJsonNull) {
                    "-"
                } else it.asJsonObject.get("NIK").asString
                surat.nama = if (it.asJsonObject.get("Nama_Lengkap").isJsonNull) {
                    "-"
                } else it.asJsonObject.get("Nama_Lengkap").asString
                surat.desa = if (it.asJsonObject.get("ds_nama").isJsonNull) {
                    "-"
                } else it.asJsonObject.get("ds_nama").asString
                surat.tipe_name = it.asJsonObject.get("st_nama").asString
                surat.formentry = it.asJsonObject.get("tr_formentry").asString.replace("\\","")
                surat.st_persyaratan = it.asJsonObject.get("st_persyaratan").asString
                surat.st_ttd = it.asJsonObject.get("st_ttd").asString
                surat.routing = it.asJsonObject.get("tr_routing").asString
                surat.attach = it.asJsonObject.get("tr_attach").asString.replace("\\","")
                val catatan = if (it.asJsonObject.has("tr_catatan")) {
                    if (!it.asJsonObject.get("tr_catatan").isJsonNull) {
                        it.asJsonObject.get("tr_catatan").asString.replace("\\","")
                    } else ""
                } else {
                    ""
                }
                val catatanJSON = if (catatan == "") {
                    JSONObject()
                } else JSONObject(catatan)
                surat.catatan = if (catatanJSON.has("catatan")) {
                    catatanJSON.getString("catatan")
                } else {
                    ""
                }
                surat.ttd = if (it.asJsonObject.get("tr_ttd").isJsonNull) {
                    ""
                } else {
                    it.asJsonObject.get("tr_ttd").asString
                }
                surat.tipe = it.asJsonObject.get("sr_id").asInt

                var status = 0
                if (it.asJsonObject.get("tr_routing").asString != "") {
                    val jsonStr = it.asJsonObject.get("tr_routing").asString.replace("\\","")
                    val obj_rout = JSONObject(jsonStr)
                    if (obj_rout.has(golongan.toString())) {
                        val stat = obj_rout.getString(golongan.toString())
                        status = if (stat == "Y") {
                            1
                        } else {
                            0
                        }
                    } else {
                        0
                    }
                } else {
                    status = 0
                }

//                surat.status_approve = if (catatan != "") {
//                    2
//                } else status

                surat.status_approve = status

                skus.add(surat)
            }
            db.suratDAO().insertLists(skus)

            uiThread {
                callback(true)
            }
        }
    }

    override fun countDashboard(callback: (HashMap<String, Int>) -> Unit) {
        doAsync {
            val map = HashMap<String, Int>()

            val sku = db.suratDAO().byTipe(1, 0)
            val sku_app = db.suratDAO().byTipe(1, 1)
            map["sku"] = sku.size
            map["sku_app"] = sku_app.size

            val skjd = db.suratDAO().byTipe(2, 0)
            val skjd_app = db.suratDAO().byTipe(2, 1)
            map["skjd"] = skjd.size
            map["skjd_app"] = skjd_app.size

            val sktm = db.suratDAO().byTipe(3, 0)
            val sktm_app = db.suratDAO().byTipe(3, 1)
            map["sktm"] = sktm.size
            map["sktm_app"] = sktm_app.size

            val skk = db.suratDAO().byTipe(4, 0)
            val skk_app = db.suratDAO().byTipe(4, 1)
            map["skk"] = skk.size
            map["skk_app"] = skk_app.size

            val skkm = db.suratDAO().byTipe(5, 0)
            val skkm_app = db.suratDAO().byTipe(5, 1)
            map["skkm"] = skkm.size
            map["skkm_app"] = skkm_app.size

            val skbm = db.suratDAO().byTipe(6, 0)
            val skbm_app = db.suratDAO().byTipe(6, 1)
            map["skbm"] = skbm.size
            map["skbm_app"] = skbm_app.size

            val skl = db.suratDAO().byTipe(7, 0)
            val skl_app = db.suratDAO().byTipe(7, 1)
            map["skl"] = skl.size
            map["skl_app"] = skl_app.size

            val skb = db.suratDAO().byTipe(8, 0)
            val skb_app = db.suratDAO().byTipe(8, 1)
            map["skb"] = skb.size
            map["skb_app"] = skb_app.size

            val skdu = db.suratDAO().byTipe(9, 0)
            val skdu_app = db.suratDAO().byTipe(9, 1)
            map["skdu"] = skdu.size
            map["skdu_app"] = skdu_app.size

            val skdp = db.suratDAO().byTipe(10, 0)
            val skdp_app = db.suratDAO().byTipe(10, 1)
            map["skdp"] = skdp.size
            map["skdp_app"] = skdp_app.size

            val spkk = db.suratDAO().byTipe(11, 0)
            val spkk_app = db.suratDAO().byTipe(11, 1)
            map["spkk"] = spkk.size
            map["spkk_app"] = spkk_app.size

            val spktp = db.suratDAO().byTipe(12, 0)
            val spktp_app = db.suratDAO().byTipe(12, 1)
            map["spktp"] = spktp.size
            map["spktp_app"] = spktp_app.size

            val sptdp = db.suratDAO().byTipe(13, 0)
            val sptdp_app = db.suratDAO().byTipe(13, 1)
            map["sptdp"] = sptdp.size
            map["sptdp_app"] = sptdp_app.size

            val spsiup = db.suratDAO().byTipe(14, 0)
            val spsiup_app = db.suratDAO().byTipe(14, 1)
            map["spsiup"] = spsiup.size
            map["spsiup_app"] = spsiup_app.size

            val simb = db.suratDAO().byTipe(15, 0)
            val simb_app = db.suratDAO().byTipe(15, 1)
            map["simb"] = simb.size
            map["simb_app"] = simb_app.size

            val spd = db.suratDAO().byTipe(16, 0)
            val spd_app = db.suratDAO().byTipe(16, 1)
            map["spd"] = spd.size
            map["spd_app"] = spd_app.size

            uiThread {
                callback(map)
            }
        }
    }

    override fun loadProfile(callback: (String, String, String) -> Unit){
        callback(pref_profile.getString("nama", "Loading ...")!!,
                pref_profile.getString("golongan_str", "Loading ...")!!,
                pref_profile.getString("desa_str", "Loading ...")!!)
    }

    override fun loadSurat(callback : (List<MasterSurat>) -> Unit) {
        doAsync {
            val data = db.masterSuratDAO().all()

            uiThread {
                callback(data)
            }
        }
    }
}