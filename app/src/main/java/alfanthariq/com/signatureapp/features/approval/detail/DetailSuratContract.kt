package alfanthariq.com.signatureapp.features.approval.detail

import alfanthariq.com.signatureapp.data.model.Surat
import alfanthariq.com.signatureapp.features.base.BaseMvpView
import alfanthariq.com.signatureapp.features.base.BasePresenter
import android.graphics.Bitmap
import android.net.Uri

object DetailSuratContract {
    interface View : BaseMvpView{

    }

    interface Presenter : BasePresenter<View>{
        fun getData(id : Int, callback : (Surat) -> Unit)
        fun approved(tr_id : Int, nik : String, ttd : Bitmap, callback: (Boolean, String) -> Unit)
        fun reject(tr_id : Int, catatan : String, callback: (Boolean, String) -> Unit)
    }
}