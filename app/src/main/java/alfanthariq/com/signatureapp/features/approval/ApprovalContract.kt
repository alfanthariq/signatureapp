package alfanthariq.com.signatureapp.features.approval

import alfanthariq.com.signatureapp.data.model.*
import alfanthariq.com.signatureapp.features.base.BaseMvpView
import alfanthariq.com.signatureapp.features.base.BasePresenter

object ApprovalContract {
    interface View : BaseMvpView {
        fun onLoadSurat(data : List<SuratList>)
        fun onSuccessPrint(url : String)
    }

    interface Presenter : BasePresenter<View> {
        //fun loadSurat(tipe : Int, callback : (List<Surat>) -> Unit)
        //fun loadOneSurat(tipe : Int, nomor_surat : String, callback : (Surat) -> Unit)
        fun loadSurat(tipe : Int)
        fun searchSurat(tipe : Int, keyword : String)
        fun print(tr_id : Int)
    }
}