package alfanthariq.com.signatureapp.features.approval

import alfanthariq.com.signatureapp.R
import alfanthariq.com.signatureapp.data.model.SuratList
import alfanthariq.com.signatureapp.features.approval.viewbinder.HeaderViewBinder
import alfanthariq.com.signatureapp.features.approval.viewbinder.SectionViewBinder
import alfanthariq.com.signatureapp.features.base.BaseActivity
import alfanthariq.com.signatureapp.features.common.ErrorView
import alfanthariq.com.signatureapp.util.AppRoute
import alfanthariq.com.signatureapp.util.NetworkUtil
import alfanthariq.com.signatureapp.util.TextWatcherHelper
import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import com.livinglifetechway.k4kotlin.onClick
import kotlinx.android.synthetic.main.activity_approval.*
import me.drakeet.multitype.ClassLinker
import me.drakeet.multitype.MultiTypeAdapter
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class ApprovalActivity : BaseActivity<ApprovalContract.View,
        ApprovalPresenter>(),
        ApprovalContract.View, ErrorView.ErrorListener {

    private var tipe = 0
    private val adapter = MultiTypeAdapter()
    private val datas = ArrayList<SuratList>()

    override var mPresenter: ApprovalPresenter
        get() = ApprovalPresenter(this)
        set(value) {}

    override fun layoutId(): Int = R.layout.activity_approval

    override fun getTagClass(): String = javaClass.simpleName

    override fun onReloadData() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        tipe = intent.getStringExtra("tipe").toInt()

        init()
    }

    override fun onResume() {
        super.onResume()
        mPresenter.loadSurat(tipe)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onLoadSurat(data: List<SuratList>) {
        datas.clear()
        datas.addAll(data)
        adapter.notifyDataSetChanged()
        hideLoadingDialog()
        swipe_surat.isRefreshing = false
    }

    override fun onSuccessPrint(url: String) {
        hideLoadingDialog()
        if (url.isNotEmpty()) {
            NetworkUtil.downloadFile(this, url)
        }
    }

    fun init(){
        setToolbar(toolbar_approval, "", true)
        toolbar_approval.setNavigationIcon(R.mipmap.ic_back)

        setupRecycler()

        mPresenter.loadSurat(tipe)

        swipe_surat.setOnRefreshListener {
            swipe_surat.isRefreshing = true
            mPresenter.loadSurat(tipe)
        }

        val tw_cari = TextWatcherHelper()
        val tw_cari_listener : TextWatcherHelper.TextWatcherListener = object : TextWatcherHelper.TextWatcherListener {
            override fun onAfterChange(editable: Editable) {
                val txt = editable.toString()
                mPresenter.searchSurat(tipe, "%$txt%")
            }
        }
        tw_cari.setListener(tw_cari_listener)
        edit_search_surat.addTextChangedListener(tw_cari)
    }

    fun setupRecycler(){
        val sectionView = SectionViewBinder({
            val param = HashMap<String, String>()
            param["surat_id"] = it.id.toString()
            param["surat_nik"] = it.nik
            AppRoute.openWithParam(this, "detail", param)
        },{ surat, opsi ->
            when (opsi) {
                0 -> {
                    showLoadingDialog("Generating ...")
                    mPresenter.print(surat.id)
                }
            }
        })
        adapter.register(SuratList::class.java).to(
                HeaderViewBinder(),
                sectionView
        ).withClassLinker(ClassLinker { position, t ->
            if (t.dataType == 1) {
                return@ClassLinker SectionViewBinder::class.java
            } else {
                return@ClassLinker HeaderViewBinder::class.java
            }
        })
        recycler_surat.layoutManager = LinearLayoutManager(this)
        recycler_surat.adapter = adapter

        adapter.items = datas
    }
}
