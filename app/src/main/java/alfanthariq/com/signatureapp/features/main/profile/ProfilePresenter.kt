package alfanthariq.com.signatureapp.features.main.profile

import alfanthariq.com.signatureapp.data.local.AppDatabase
import alfanthariq.com.signatureapp.data.remote.ApiClient
import alfanthariq.com.signatureapp.data.remote.ApiService
import alfanthariq.com.signatureapp.features.base.BasePresenterImpl
import alfanthariq.com.signatureapp.util.FileUtil
import alfanthariq.com.signatureapp.util.NetworkUtil
import alfanthariq.com.signatureapp.util.PreferencesHelper
import android.graphics.Bitmap
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfilePresenter (var view : ProfileContract.View) :
        BasePresenterImpl<ProfileContract.View>(), ProfileContract.Presenter{

    val context = view.getContext()
    var apiService: ApiService? = null
    val db = AppDatabase.getInstance(context)!!
    val pref_profile = PreferencesHelper.getProfilePref(context)
    val nik = pref_profile.getString("nik", "")
    val userid = pref_profile.getInt("user_id", 0)

    val token = pref_profile.getString("token", "")

    override fun updateTTD(ttdBitmap: Bitmap, callback: (Boolean, String) -> Unit) {
        apiService = ApiClient.getClient(context, null, NetworkUtil.useAPI)
                .create(ApiService::class.java)
        val signStr = FileUtil.convert(ttdBitmap)
        val sendApi = apiService?.updateTTD(nik, userid, signStr, token)
        sendApi?.enqueue(object : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                callback(false, "Request failed")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val json: String = response.body()!!.string()
                    val obj: JsonObject = JsonParser().parse(json).asJsonObject
                    val sukses = obj.get("success").asBoolean

                    if (sukses) {
                        val editor = pref_profile.edit()
                        editor.putString("ttd_base64", signStr)
                        editor.apply()
                        callback(true, "")
                    }
                } else {
                    callback(false, "Gagal update template tanda tangan, mohon coba kembali")
                }
            }

        })
    }

}