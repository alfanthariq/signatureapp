package alfanthariq.com.signatureapp.features.main

import alfanthariq.com.signatureapp.data.model.MasterSurat
import alfanthariq.com.signatureapp.features.base.BaseMvpView
import alfanthariq.com.signatureapp.features.base.BasePresenter

object MainContract {
    interface View : BaseMvpView {

    }

    interface Presenter : BasePresenter<View> {
        fun logout(callback: (Boolean) -> Unit)
        fun downloadSurat(callback : (Boolean) -> Unit)
        fun countDashboard(callback: (HashMap<String, Int>) -> Unit)
        fun loadProfile(callback: (String, String, String) -> Unit)
        fun loadSurat(callback : (List<MasterSurat>) -> Unit)
    }
}