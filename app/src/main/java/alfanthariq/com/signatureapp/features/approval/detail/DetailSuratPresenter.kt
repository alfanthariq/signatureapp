package alfanthariq.com.signatureapp.features.approval.detail

import alfanthariq.com.signatureapp.data.local.AppDatabase
import alfanthariq.com.signatureapp.data.model.MasterSurat
import alfanthariq.com.signatureapp.data.model.Surat
import alfanthariq.com.signatureapp.data.remote.ApiClient
import alfanthariq.com.signatureapp.data.remote.ApiService
import alfanthariq.com.signatureapp.features.base.BasePresenterImpl
import alfanthariq.com.signatureapp.util.DateOperationUtil
import alfanthariq.com.signatureapp.util.FileUtil
import alfanthariq.com.signatureapp.util.NetworkUtil
import alfanthariq.com.signatureapp.util.PreferencesHelper
import android.graphics.Bitmap
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import okhttp3.ResponseBody
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailSuratPresenter(var view : DetailSuratContract.View)
    : BasePresenterImpl<DetailSuratContract.View>(), DetailSuratContract.Presenter{

    val context = view.getContext()
    var apiService: ApiService? = null
    val db = AppDatabase.getInstance(context)!!
    val pref_profile = PreferencesHelper.getProfilePref(context)

    val gol = pref_profile.getInt("golongan_id", 0)
    val token = pref_profile.getString("token", "")

    override fun getData(id: Int, callback: (Surat) -> Unit) {
        doAsync {
            val data = db.suratDAO().one(id)

            uiThread {
                callback(data)
            }
        }
    }

    override fun approved(tr_id: Int, nik : String, ttd: Bitmap, callback: (Boolean, String) -> Unit) {
        apiService = ApiClient.getClient(context,null, NetworkUtil.useAPI)
                .create(ApiService::class.java)
        val signStr = FileUtil.convert(ttd)
        val sendApi = apiService?.approveSurat(tr_id, gol, nik, signStr, token)
        sendApi?.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                callback(false, "Request failed")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    doAsync {
                        val json: String = response.body()!!.string()
                        val obj: JsonObject = JsonParser().parse(json).asJsonObject
                        val sukses = obj.get("success").asBoolean

                        if (sukses) {
                            db.suratDAO().updateStatus(tr_id, 1)

                            uiThread {
                                redownloadSurat(tr_id, callback)
                            }
                        } else {
                            uiThread {
                                callback(false, "Request failed")
                            }
                        }
                    }
                } else {
                    val json: String = response.errorBody()!!.string()
                    val obj: JsonObject = JsonParser().parse(json).asJsonObject
                    val err_msg = obj.get("err_message").asString
                    callback(false, err_msg)
                }
            }
        })
    }

    fun redownloadSurat(id : Int, callback: (Boolean, String) -> Unit){
        apiService = ApiClient.getClient(context,null, NetworkUtil.useAPI)
                .create(ApiService::class.java)
        val sendApi = apiService?.getSurat(id, token)
        sendApi?.enqueue(object : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                callback(true, "")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    println("response success")
                    val json: String = response.body()!!.string()
                    val obj: JsonObject = JsonParser().parse(json).asJsonObject

                    insertSurat(obj, callback)
                } else {
                    callback(true, "")
                }
            }
        })
    }

    fun insertSurat(obj : JsonObject, callback: (Boolean, String) -> Unit){
        doAsync {
            db.suratDAO().deleteAll()
            val data_sku = obj.get("data").asJsonArray
            val skus = ArrayList<Surat>()
            data_sku.forEach {
                val surat = Surat()
                surat.nomor_surat = it.asJsonObject.get("sr_name").asString
                surat.id = it.asJsonObject.get("tr_id").asInt
                surat.tanggal = DateOperationUtil.dateStrFormat("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", it.asJsonObject.get("tr_tglcreated").asString)
                surat.nik = it.asJsonObject.get("tr_nik").asString
                surat.tipe_name = it.asJsonObject.get("st_nama").asString
                surat.formentry = it.asJsonObject.get("tr_formentry").asString
                surat.st_persyaratan = it.asJsonObject.get("st_persyaratan").asString
                surat.st_ttd = it.asJsonObject.get("st_ttd").asString
                surat.routing = it.asJsonObject.get("tr_routing").asString
                surat.attach = it.asJsonObject.get("tr_attach").asString
                surat.catatan = it.asJsonObject.get("tr_catatan").asString
                surat.ttd = if (it.asJsonObject.get("tr_ttd").isJsonNull) {
                    ""
                } else {
                    it.asJsonObject.get("tr_ttd").asString
                }
                surat.tipe = it.asJsonObject.get("sr_id").asInt
                var status = 0
                if (it.asJsonObject.get("tr_routing").asString != "") {
                    val jsonStr = it.asJsonObject.get("tr_routing").asString.replace("\\","")
                    val obj_rout = JSONObject(jsonStr)
                    if (obj_rout.has(gol.toString())) {
                        val stat = obj_rout.getString(gol.toString())
                        status = if (stat == "Y") {
                            1
                        } else {
                            0
                        }
                    } else {
                        0
                    }
                } else {
                    status = 0
                }

                surat.status_approve = status

                skus.add(surat)
            }
            db.suratDAO().insertLists(skus)

            uiThread {
                callback(true, "")
            }
        }
    }

    override fun reject(tr_id: Int, catatan: String, callback: (Boolean, String) -> Unit) {
        apiService = ApiClient.getClient(context,null, NetworkUtil.useAPI)
                .create(ApiService::class.java)
        val sendApi = apiService?.rejectSurat(tr_id, catatan, token)
        sendApi?.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                callback(false, "Request failed")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    doAsync {
                        val json: String = response.body()!!.string()
                        val obj: JsonObject = JsonParser().parse(json).asJsonObject
                        val sukses = obj.get("success").asBoolean

                        if (sukses) {
                            db.suratDAO().updateStatus(tr_id, 2)
                            db.suratDAO().updateCatatan(tr_id, catatan)

                            uiThread {
                                callback(true, "")
                            }
                        } else {
                            uiThread {
                                callback(false, "Request failed")
                            }
                        }
                    }
                } else {
                    val json: String = response.errorBody()!!.string()
                    val obj: JsonObject = JsonParser().parse(json).asJsonObject
                    val err_msg = obj.get("err_message").asString
                    callback(false, err_msg)
                }
            }
        })
    }
}